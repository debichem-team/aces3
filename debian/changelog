aces3 (3.0.8-7) UNRELEASED; urgency=medium

  [ Michael Banck ]
  * debian/rules (override_dh_auto_clean): Run dh_auto_clean as first command. 
    (override_dh_auto_test): Replaced OMPI_MCA_plm_orte_agent variable with
    OMPI_MCA_plm_rsh_agent.
  * debian/patches/fix_geopt_finalize_crash.patch: New patch, removes an abort
    at the end of geometry optimizations, fixing test case 1.2.1.1.
  * debian/patches/disable_f12_code.patch: New patch, disabled the
    excplicitly-correlated code for now, thanks to Jason Byrd.
  * debian/patches/fix_autoconf.patch: Removed, no longer needed.
  * debian/patches/testsuite_quicktests.patch: Reduce waiting between tests to
    one second.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 07 Feb 2016 19:09:44 +0100

aces3 (3.0.8-6) unstable; urgency=medium

  * Rebuild against latest openmpi to fix FTBFS on mult. arches
  * Standards-Version: 4.2.1
  * make build more verbose by default
  * Point VCS-* to salsa.d.o, homepage to https version
  * Set aces3-data to Multi-Arch: foreign

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 04 Dec 2018 16:21:47 +0000

aces3 (3.0.8-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix obsolete use of libgfortranbegin. Closes: #831164.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 10 Sep 2016 11:05:40 +0100

aces3 (3.0.8-5) unstable; urgency=medium

  * debian/patches/makefile_show_output.patch: New patch, makes the Makefiles
    no longer supress general output and errors (Closes: #794782).
  * debian/rules (override_dh_auto_clean): Remove config.h.
  * debian/control (DM-Upload-Allowed): Removed.
  * debian/control (Standards-Version): Bumped to 3.9.6.

 -- Michael Banck <mbanck@debian.org>  Sun, 22 Nov 2015 17:53:16 +0100

aces3 (3.0.8-4) unstable; urgency=medium

  * debian/rules (FFLAGS1): Remove quotes (Closes: #767498).

 -- Michael Banck <mbanck@debian.org>  Sun, 09 Nov 2014 17:28:11 +0100

aces3 (3.0.8-3) unstable; urgency=medium

  * debian/rules: Move FFLAGS1 from override_dh_auto_configure to a top-level
    variable (Closes: #767498). 

 -- Michael Banck <mbanck@debian.org>  Sun, 09 Nov 2014 10:05:12 +0100

aces3 (3.0.8-2) unstable; urgency=medium

  * debian/rules (override_dh_auto_configure): Set FFLAGS1 for F90 source
    files, including -ffunction-sections to fix FTBFS errors on armhf and
    arm64 (Closes: #767498).
  * debian/rules (override_dh_auto_configure): Remove superfluous -lmpi_cxx and
    -lstdc++ from GNULIBS, fixing an FTBFS error on s390x (Closes: #767498).
  * debian/control (aces3-data): Add Replaces for aces3 package prior to the
    -data splitup (Closes: #768212).

 -- Michael Banck <mbanck@debian.org>  Sat, 08 Nov 2014 13:50:23 +0100

aces3 (3.0.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/default_directories.patch: Updated.
  * debian/patches/testsuite_input.patch: Likewise.
  * debian/patches/makefile_distclean.patch: Likewise.
  * debian/patches/testsuite_prepare.patch: Likewise.
  * debian/patches/divide-by-zero-error_fix.patch: Likewise.
  * debian/patches/exit_impossible_seq_jobs_gracefully.patch: Likewise.
  * debian/patches/ignore_invalid_message_type_nind.patch: Likewise.
  * debian/patches/fix_format_not_string.patch: Likewise.
  * debian/patches/testsuite_quicktests.patch: Likewise.
  * debian/patches/fix_binary_characters_in_output.patch: Likewise.
  * debian/patches/testsuite_compare.patch: Likewise.
  * debian/patches/compile_error.patch: Removed, no longer needed.
  * debian/patches/fix_compile_errors.patch: New patch, fixes compile errors
    introduced in new upstream version.
  * debian/patches/fix_autoconf.patch: New patch, fixes issues with AC_SUBST
    ordering and adds some missing Makefiles to AC_CONFIG_FILES.
  * debian/rules (override_dh_auto_clean, override_dh_auto_configure): Adjust
    for new location of dup directory and include a couple of more Fortran
    files which are newly required in the new upstream version.
  * debian/rules (override_dh_auto_configure): Run dh_auto_configure.
  * debian/patches/patches/testsuite_quicktests.patch: Delete newly created
    runtests-quick script as well.
  * debian/patches/makefile_distclean.patch: Removed Fortran90 .mod files from
    oed_F12 and erd_F12 directories, as well as a couple of new uncleaned files.
  * debian/rules (override_dh_auto_clean): Remove auto-generated Makefiles.
  * debian/patches/makeflags.patch: Removed, no longer needed.
  * debian/patches/fix_read_after_eof.patch: New patch, fixes runtime errors
    when writing ZMAT.AUTO.
  * debian/patches/fix_corrupted_jobabort_variable.patch: New patch, fixes a
    memory corruption leading to early aborts of compute jobs.
  * debian/patches/update_version.patch: New patch, update version number.
  * debian/patches/mem_alloc.patch: New patch, replaces the new and 64bit-only
    memory allocation with the code from version 3.0.7.
  * debian/rules (override_dh_auto_clean): Make sure all .sio source files are
    removed on clean.

 -- Michael Banck <mbanck@debian.org>  Sun, 26 Oct 2014 18:10:40 +0100

aces3 (3.0.7-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/makeflags.patch: Updated.
  * debian/patches/makefile_distclean.patch: Likewise.
  * debian/patches/exit_impossible_seq_jobs_gracefully.patch: Likewise.
  * debian/patches/fix_binary_characters_in_output.patch: Likewise.
  * debian/patches/compile_error.patch: New patch, fixes compile errors.
  * debian/control (aces3/Depends): Fixed depends on aces3-data.

 -- Michael Banck <mbanck@debian.org>  Mon, 13 Oct 2014 13:25:30 +0200

aces3 (3.0.6-8) unstable; urgency=medium

  [ Michael Banck ]
  * debian/patches/testsuite_input.patch: Updated all testsuite input files
    with much less demanding H2O coordinates and VDZ basis sets.
  * debian/patches/testsuite_prepare.patch: Run testsuite with two MPI
    processes.  Otherwise, most tests abort.
  * debian/patches/testsuite_quicktests.patch: New patch, define a new set
    $(QUICKTESTS) of tests which only runs a few tests to check basic
    functionality.  Also create a runscript-quick to run them.
  * debian/patches/testsuite_prepare.patch: Adjusted to no longer modify the
    set of tests in $(TESTS).
  * debian/rules (override_dh_auto_test): Run runscript-quick instead of
    runcript.
  * debian/patches/fix_binary_characters_in_output.patch: New patch, crops the
    sial_program variable at 60 characters, avoiding binary garbage at the end
    which makes the ACESIII output files be recognized as binary files by Unix
    tools (Closes: #687287).
  * debian/aces3.install: Install SIO into /usr/share, not /usr/lib, as they
    are architecture-independent.
  * debian/patches/default_directories.patch: Adjusted for new location of the
    SIO files.
  * debian/control (aces3-data): New package.
  * debian/control (aces3/Depends): Depend on it.
  * debian/aces3-data.install: New file, install the SIO and GENBAS files.
  * debian/aces3.install: No longer install SIO and GENBAS files.
  * debian/docs: Renamed to...
  * debian/aces3-data.docs: ...this.
  * debian/upstream (Name, Registration): New fields.
  * debian/upstream (References): Added Comp. Mol. Sci. review.
  * debian/patches/testsuite_compare.patch: New patch, updates the reference
    test suite results with values taken from ACES II calculations on the new
    testsuite inputs from debian/patches/testsuite_input.patch.  In case no ACES
    II results were available (for the test cases 1.3.2.1, 3.1.4.1, 3.1.4.2,
    3.2.2.1 and 3.2.2.2), values from ACES III on amd64 were taken.
  * debian/aces3.install: Install the SIAL compiler into /usr/bin as well.
  * debian/rules (override_dh_auto_test): Update OMPI_MCA environment variable
    to orte_rsh_agent as the original plm_rsh_agent variable is deprecated.
  * debian/patches/makeflags.patch: Added -fno-aggressive-loop-optimizations to
    FFLAGS.
  * debian/upstream: Moved to debian/upstream/metadata.

  [ Andreas Tille ]
  * debian/upstream: New file.

 -- Michael Banck <mbanck@debian.org>  Mon, 13 Oct 2014 00:56:11 +0200

aces3 (3.0.6-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix format not a string FTBFS. (Closes: #748769)

 -- Anton Gladky <gladk@debian.org>  Mon, 26 May 2014 19:52:51 +0200

aces3 (3.0.6-7) unstable; urgency=high

  [ Michael Banck ]
  * debian/patches/exit_impossible_seq_jobs_gracefully.patch: New patch,
    prints a helpful error message and quits if a job type is requested which
    cannot be run sequentially (Closes: #687266).
  * debian/patches/ignore_invalid_message_nind.patch: New patch, if a message
    of type ``server_barrier_signal'' or ``server_quit_msgtype'' arrives,
    ignore if the value of the ``nind'' field is invalid (Closes: #687264).

 -- Michael Banck <mbanck@debian.org>  Mon, 22 Apr 2013 10:44:02 +0200

aces3 (3.0.6-6) unstable; urgency=low

  * debian/patches/divide-by-zero-error_fix.patch: New patch, fixes a
    floating-point exception in case some types of calculations are run on
    a single core (Closes: #683467).

 -- Michael Banck <mbanck@debian.org>  Sun, 09 Sep 2012 09:05:06 +0200

aces3 (3.0.6-5) unstable; urgency=low

  [ Michael Banck ]
  * debian/rules (override_dh_auto_clean, override_dh_auto_configure): New
    rules, moving away duplicate lapack/blas source files in the dup/
    subdirectory on configure and putting them back in place on clean 
    (Closes: #680397).
  * debian/patches/testsuite_prepare.patch: Further cut down the testsuite to
    only test case 1.1.1.1 in order to relief slower buildds.

  [ Daniel Leidert ]
  * debian/watch: Added.

 -- Michael Banck <mbanck@debian.org>  Thu, 19 Jul 2012 02:08:21 +0200

aces3 (3.0.6-4) unstable; urgency=low

  * debian/patches/patches/default_directories.patch: Fix patch for the case
    when $ACES_EXE_PATH is not set.
  * debian/rules (override_dh_auto_test): Only run testsuite if
    DEB_BUILD_OPTIONS does not contain `nocheck', as per policy.

 -- Michael Banck <mbanck@debian.org>  Sat, 24 Mar 2012 04:01:26 +0100

aces3 (3.0.6-3) unstable; urgency=low

  * debian/patches/patches/default_directories.patch: Rework patch to not fail
    if $ACES_EXE_PATH is set but /usr/lib/aces3 is not present.

 -- Michael Banck <mbanck@debian.org>  Sat, 24 Mar 2012 02:53:51 +0100

aces3 (3.0.6-2) unstable; urgency=low

  * debian/control (Description): Slight updates.
  * debian/rules (override_dh_auto_test): Set OMPI_MCA_plm_rsh_agent
    environment variable to make test suite run in a chroot.
  * debian/patches/testsuite_prepare.patch: Remove most tests from TESTS
    varilable to make testsuite run shorter, echo current test name to stdout
    to produce some output and remove calls to the non-existant `lfs' utility.

 -- Michael Banck <mbanck@debian.org>  Sat, 24 Mar 2012 01:53:34 +0100

aces3 (3.0.6-1) unstable; urgency=low

  * Initial upload (Closes: #640718).

 -- Michael Banck <mbanck@debian.org>  Tue, 06 Sep 2011 22:05:11 +0200
